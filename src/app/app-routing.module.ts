import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewFilmComponent } from './new-film/new-film.component';
import { FilmListComponent } from './film-list/film-list.component';

const routes: Routes = [
  {path: 'new', component: NewFilmComponent},
  {path: ':id', component: FilmListComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
