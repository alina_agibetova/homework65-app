import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FilmService } from '../shared/film.service';
import { Film } from '../shared/film.modal';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-film',
  templateUrl: './new-film.component.html',
  styleUrls: ['./new-film.component.css']
})
export class NewFilmComponent implements OnInit, OnDestroy {
  nameInput = '';
  isFetching = false;
  filmsFetchingSubscription!: Subscription;

  constructor(private http: HttpClient, private filmService: FilmService) { }

  ngOnInit(): void {
    this.filmsFetchingSubscription = this.filmService.filmsFetching.subscribe(isFetching => {
      this.isFetching = isFetching;
    })
  }

  createFilmPost(){
    const name = this.nameInput;
    const body = {name};
    const id = Math.random().toString();
    const newFilm = new Film(id, name);
    this.filmService.addFilm(newFilm).subscribe(() => {
      this.filmService.fetchFilms();
    });
  }

  ngOnDestroy(): void {
    this.filmsFetchingSubscription.unsubscribe();
  }

}
