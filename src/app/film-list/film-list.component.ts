import { Component, OnDestroy, OnInit } from '@angular/core';
import { Film } from '../shared/film.modal';
import { Subscription } from 'rxjs';
import { FilmService } from '../shared/film.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css']
})
export class FilmListComponent implements OnInit, OnDestroy {
  films!: Film[];
  film!: Film;
  filmsChangeSubscription!: Subscription;
  filmsFetchingSubscription!: Subscription;
  isFetching: boolean = false;

  constructor(private filmService: FilmService, private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.films = this.filmService.getFilms();
    this.filmsChangeSubscription = this.filmService.filmsChange.subscribe((films: Film[]) => {
      this.films = films;
    });
    this.filmsFetchingSubscription = this.filmService.filmsFetching.subscribe((isFetching: boolean)=> {
      this.isFetching = isFetching;
    });

    this.filmService.fetchFilms();
  }

  ngOnDestroy(){
    this.filmsChangeSubscription.unsubscribe();
    this.filmsFetchingSubscription.unsubscribe();

  }

  onDelete(filmId: string) {
    this.filmService.deleteFilm(filmId).subscribe(() => {
      this.filmService.fetchFilms();
    });
  }
}
