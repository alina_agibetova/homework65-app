import { Subject } from 'rxjs';
import { Film } from './film.modal';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Injectable()
export class FilmService {
  filmsChange = new Subject<Film[]>();
  filmsFetching = new Subject<boolean>();


  constructor(private http: HttpClient, private route: ActivatedRoute){}

  fetchFilms(){
    this.filmsFetching.next(true);
    this.http.get<{[id: string]: Film}>(`https://alina-beaf9-default-rtdb.firebaseio.com/films.json`)
      .pipe(map(result => {
        return Object.keys(result).map(id =>{
          const filmData = result[id];

          return new Film(id, filmData.name);
        })
      }))
      .subscribe(films =>{
        this.films = films;
        this.filmsChange.next(this.films.slice());
        this.filmsFetching.next(false);
      });
    }
    private films: Film[] = [];

  getFilms(){
    return this.films.slice();
  }

  addFilm(film: Film){
    this.filmsFetching.next(true);
    const body = {
      name: film.name,
    };
    return this.http.post('https://alina-beaf9-default-rtdb.firebaseio.com/films.json', body).pipe(
      tap(() => {
        this.filmsFetching.next(false);
      }, () => {
        this.filmsFetching.next(false);
      })
    );
  }


  deleteFilm(filmId: string){
    this.filmsFetching.next(true);
    return this.http.delete(`https://alina-beaf9-default-rtdb.firebaseio.com/films/${filmId}.json`).pipe(
      tap(() => {
        this.filmsFetching.next(false);
      }, () => {
        this.filmsFetching.next(false);
      })
    );
  }
}
